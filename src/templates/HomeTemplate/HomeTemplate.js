import { Fragment } from "react";
import { Route } from "react-router-dom";
import HomeCarousel from "./Layout/Carousel/HomeCarousel";
import Footer from "./Layout/Footer/Footer";
import Header from "./Layout/Header/Header";

export const HomeTemplate = (props) => {
  const { Component, ...restProps } = props;

  return (
    <Route
      {...restProps}
      render={(propsRoute) => {
        return (
          <Fragment>
            <Header {...propsRoute} />
            <HomeCarousel {...propsRoute} />
            <Component {...propsRoute} />
            <footer>
              <Footer />
            </footer>
          </Fragment>
        );
      }}
    />
  );
};
