import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { carouselService } from "../../../../services/carouselService";

const contentStyle = {
  height: "600px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  //background: "#364d79",
  backgroundPosition: "center",
  backgroundSize: "100%",
  backgroundRepeat: "no-repeat",
};

export default function HomeCarousel(props) {
  const [carousel, setCarousel] = useState([]);
  useEffect(() => {
    carouselService
      .getCarouselMovie()
      .then((res) => {
        setCarousel(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  // console.log("arrImg: ", arrImg);

  const renderImg = () => {
    return carousel.map((item, index) => {
      return (
        <div key={index}>
          <div
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img
              src={item.hinhAnh}
              className="w-full opacity-0"
              alt={item.hinhAnh}
            />
          </div>
        </div>
      );
    });
  };

  return <Carousel autoplay>{renderImg()}</Carousel>;
}
