import React, { useEffect, useState } from "react";
import HomeMenu from "./HomeMenu/HomeMenu";
import { Card } from "antd";
import { moviesServ } from "../../services/moviesService";
import { Carousel } from "antd";
import Slider from "react-slick";

const { Meta } = Card;

const settings = {
  className: "center",
  centerMode: true,
  infinite: true,
  centerPadding: "60px",
  slidesToShow: 3,
  speed: 500,
  rows: 2,
  slidesPerRow: 2,
};

const contentItemCarouselStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

export default function HomePage(props) {
  //get movie item from API
  const [itemMovie, setItemMovie] = useState([]);
  useEffect(() => {
    moviesServ
      .getListMovie()
      .then((res) => {
        setItemMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  //render movie item card
  const renderItemMovie = () => {
    return itemMovie.map((item, index) => {
      return (
        <Card
          key={index}
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img
              className="h-80 w-full object-cover"
              alt={item.moTa}
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            title={item.tenPhim}
            description={<p className="truncate">{item.moTa}</p>}
          />
          <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
            Xem Chi Tiết
          </button>
        </Card>
      );
    });
  };

  //make movie items as a carousel
  const ItemMovieCarousel = () => {
    const onChange = (currentSlide) => {
      console.log("currentSlide: ", currentSlide);
    };

    return (
      <Carousel afterChange={onChange}>
        <div style={contentItemCarouselStyle}>
          <div className="justify-center align-items:center grid grid-cols-4 gap-10">
            {renderItemMovie()}
          </div>
        </div>
      </Carousel>
    );
  };

  return (
    <div className="mt-5 mx-96">
      {ItemMovieCarousel()}
      <br />

      <div className="mx-36">
        <HomeMenu />
      </div>
    </div>
  );
}
