import React, { useEffect, useState } from "react";
import { Radio, Space, Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";
import { moviesServ } from "../../../services/moviesService";
import ItemsHomeMenu from "./ItemsHomeMenu";

export default function HomeMenu(props) {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    moviesServ
      .getMovieTheater()
      .then((res) => {
        console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const [state, setState] = useState({
    tabPosition: "left",
  });

  const changeTabPosition = (e) => {
    setState({ tabPosition: e.target.value });
  };

  const { tabPosition } = state;

  let renderMenu = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <div style={{ height: 500, overflowY: "scroll" }}>
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemsHomeMenu data={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderMenu()}
      </Tabs>
    </div>
  );
}
