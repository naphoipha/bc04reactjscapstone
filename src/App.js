import "./App.css";
import { createBrowserHistory } from "history";
import HomePage from "./pages/HomePage/HomePage";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import { Switch, Router, Route } from "react-router";
import ContactPage from "./pages/ContactPage/ContactPage";
import NewsPage from "./pages/NewsPage/NewsPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import RegisterPage from "./pages/RegisterPage/RegisterPage";

export const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <Switch>
        <HomeTemplate path="/homepage" exact Component={HomePage} />
        <HomeTemplate path="/" exact Component={HomePage} />
        <HomeTemplate path="/contactpage" exact Component={ContactPage} />
        <HomeTemplate path="/newspage" exact Component={NewsPage} />
        <Route path="/loginpage" exact Component={LoginPage} />
        <Route path="/registerpage" exact Component={RegisterPage} />
      </Switch>
    </Router>
  );
}

export default App;
