import { https } from "./configURL";

export const moviesServ = {
  getListMovie: () => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00";
    return https.get(uri);
  },

  getMovieTheater: () => {
    let uri = "api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP00";
    return https.get(uri);
  },
};
